#include <stdio.h>
int main()
{
	int a,b,*ptr1,*ptr2,temp;
	printf("Enter two elements : \n");
	scanf("%d%d",&a,&b);
	printf("Elements before swapping : %d %d",a,b);
	ptr1 = &a;
	ptr2 = &b;
	temp = *ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;
	printf("\nElements after swapping : %d %d",a,b);
	return 0;
}
